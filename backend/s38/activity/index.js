let http = require("http");
let port = 4000;

let app = http.createServer((request,response)=>{

    if(request.url == "/" && request.method == "GET"){
        response.writeHead(200,{'Content-type': 'text/plain'});
        response.write('Welcome to Booking System');
        response.end();
    }

    else if(request.url == "/profile" && request.method == "GET"){
        response.writeHead(200,{'Content-type': 'text/plain'});
        response.write('Welcom to your profile!')
        response.end();
    }
    else if(request.url == "/courses" && request.method == "GET"){
        response.writeHead(200,{'Content-type': 'text/plain'});
        response.write("Here's our courses available");
        response.end();

    }
    else if(request.url == "/addcourses" && request.method == "POST"){
        response.writeHead(200,{'Content-type': 'text/plain'});
        response.write('Add a course to our resources');
        response.end();    
    }

    else if(request.url == "/updatecourses" && request.method == "PUT"){
        response.writeHead(200,{'Content-type': 'text/plain'});
        response.write('Update a course to our resources');
        response.end();    
    }

    else if(request.url == "/archivecourses" && request.method == "DELETE"){
        response.writeHead(200,{'Content-type': 'text/plain'});
        response.write('Archive course to our resources');
        response.end();    
    }

})

app.listen(port,()=>console.log(`Server running at localhost:${port}`))

















//Do not modify
//Make sure to save the server in variable called app
if(require.main === module){
    app.listen(4000, () => console.log(`Server running at port 4000`));
}

module.exports = app;