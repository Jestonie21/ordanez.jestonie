// Syntax, Statements, and Comments


console.log("We tell the computer to log this in the console.");
alert("We tell the computer to display an alert with this message!");


// Statements - in programming are instructions that we tell the computer to perform
// JS Statements usually end with semicolon (;)

// Syntax - it is the set of rules that describes how statements must be constructed

// Comments
// For us to create a comment, we use CTrl + /
// We have single line (CTrl + /)and multi-line comments (ctrl + shift + /)

// console.log("Hello");

/*alert("This is an alert");
alert("This is another alert");*/

// Whitespaces (spaces and line breaks) can impact functionality in many computer languages, but not in JS.

// Variables
// This is used to contain data

// Declare variables
	// tell our devices that a variableName is created and is ready store data
	// Syntax
		// let/const variableName;

	let myVariable;
	console.log(myVariable);

	// Variables must be declared first before they are used
	// using variables before they are declared will return an error
	let hello;
	console.log(hello);

	// declaring and initializing variables
	// syntax
		//let/const variableName = initialValue;

	let productName = 'desktop computer';
	console.log(productName);

	let productPrice = 18999;
	console.log(productPrice);

	const interest = 3.539;

	// re-assigning variable values

	productName = 'Laptop';
	console.log(productName);

	// We cannot replace constant values
	// interest = 3.5;
	// console.log(interest);

	// let variable  cannot be re-declared within its scope
	let friend = 'Kate';
	friend = 'Jane';

	// Declare a value
	let supplier;

	// Initialization is done after the variable has been declared
	supplier = "John Smith Tradings"

	// This is considered as re-assignment because its initial value was already declared
	supplier = "Zuitt Store";

	// We cannot decclare a const variable without initialization.
	/*const pi;
	pi = 3.1416;
	console.log(pi);*/

	// Multiple variable declarations

	let productCode = 'DC017' , productBrand = 'Dell';

	// let productCode = 'DC017';
	// const productBrand = 'Dell';
	console.log(productCode, productBrand);

	// Data Types

	// Strings are a series of characters that create a word, a a phrase, or a sentence or anything related to creating text
	// Strings in JavaScript can be written using either a single (') or double (") quote
	// In other programming languages, only the double quotes can be used for creating strings

	let country = 'Philippines';
	let province = "Metro Manila";


	// Concatenenating Strings

	let fullAddress = province + ',' + country;
	console.log(fullAddress);

	let greeting = 'I live in the ' + country;
	console.log(greeting);

	// the scape character (\) in strings in combination with other characters can produce different effects

	let mailAddress = 'Metro Manila\n\n\n\nPhilippines';
	console.log(mailAddress);

	let message = "John employees went home early";
	console.log(message);
	message = 'John\'s employees went home early';
	console.log(message);

	// Numbers

	let headcount = 26;
	console.log(headcount);
	let grade = 98.7;
	console.log(grade);
	let planetDistance = 2e10;
	console.log(planetDistance);

	//Combine text and strings
	console.log("John's first grade last quarter is " + grade);

	// Boolean
	let isMarried = false;
	let isGoodConduct = true;
	console.log(isMarried);

	console.log("isGoodConduct: " + isGoodConduct);

	// Arrays 

	//Syntax
		//let/const arrayName = [elementA, elementB, ...]
	let grades = [98.7, 92.1, 90.7, 98.6];
	console.log(grades)

	// Objects

	let myGrades = {
		firstGrading: 98.7,
		secondGrading: 92.1,
		thirdGrading: 90.7,
		fourhGrading: 94.6,
	}
	console.log(myGrades);

	let person = {

		fullName: 'Juan Dela Cruz',
		age: 35,
		isMarried: false,
		contact: ["+639123456789", "8700"],
		address:{
			houseNumber: '345',
			city: 'Manila',
		}
	}
console.log(person);


//type of operator
//typeof operator is used to determine the type of data or the value of a variable. It outputs a string.

console.log(typeof person);//object

//Note: Array is a special type of object with methods and functions to manipulate it. We will discuss these methods in later sessions. (Javascript - Array Manipulation)
console.log(typeof grades);


// null
	//it is used to intentionally express the absence of the value in a variable  declartion/initialization

	let spouse = null;

// undefined
	//this represents the state of a variable that has been declared but without an assigned value

	let fullName;
	console.log(fullName);//undefined 


	console.log(dog);

	