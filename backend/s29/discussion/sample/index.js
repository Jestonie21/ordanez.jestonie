console.log("DOM Manipulation");


//const clicker = document.querySelector('#clicker');

console.log(document);
console.log(document.querySelector('#clicker'));
/*
	document - refers to the whole webpage
	querySelector - used to select a specific element (obj) as long as it is inside the html tag (html element)
	- takes a string input that if formatted like CSS selector
	- can select elements regardless if the string is an id, class, or tag as long as the element is existing
*/
/*
	Alternative methods thatt we use aside from querySelector in retrieving elements:
		document.getElementById()

*/
let counter = 0;

const clicker = document.querySelector('#clicker');

	clicker.addEventListener('click',()=>{
		console.log("The button has been clicked");
		counter++;
		alert(`The button has been clicked ${counter} times`);
	})

const txtFirstName = document.querySelector("#txt-first-name");
const txtLastName = document.querySelector("#txt-last-name");
const spanFullName = document.querySelector("#span-full-name");

const updateFullName = (e) =>{

	let firstName = txtFirstName.value;
	let lastName = txtLastName.value;

	spanFullName.innerHTML = `${firstName} ${lastName}`
}

	txtLastName.addEventListener('keyup', updateFullName);
	txtFirstName.addEventListener('keyup', updateFullName);

/*txtFirstName.addEventListener('keyup', (event)=>{
	spanFullName.innerHTML = txtFirstName.value
})

txtLastName.addEventListener('keyup', (event)=>{
	spanFullName.innerHTML = txtLastName.value
})

spanFullName.addEventListener('keydown',(event)=>{
	spanFullName.innerHTML = spanFullName.value
})
*/
const labelFirstName = document.querySelector("#label-txt-first-name");
labelFirstName.addEventListener("mouseover",(e)=>{

	alert("You hovered the First Name Label!")
})