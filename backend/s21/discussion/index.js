console.log("Hi, B297");

//Mini-Activity - log your favorite line 20 times in the console.
console.log("I'm the king of the world!");

function printline(){
	console.log("I'm the king of the world!")
};

printline();

//Functions
//lines/blocks of code that tell our devices to perform a certain task when called/invoked

//Function declaration

/*
	Syntax:

	function functionName() {
		code block (statement)
	}

*/
	//Function Declaration
	function printName(){
		console.log("My name is Jestonie.");
	};

	//Function Invocation
	printName();

	declaredFunction();

	//Function declaration vs expressions

	//Function declaration
		//function declaration is created with the function keyword and adding a function name
	//they are "saved for later use"

	function declaredFunction(){
		console.log("Hello from declaredFunction!")
	};

	declaredFunction();

	//Function Expression
		//function expression is stored in a variable
		//function ecpression is an anonymous function assigned to the variable

	//variableFunction();//Uncaught ReferenceError: Cannot access 'variableFunction' before initialization
	
	let variableFunction = function() {
		console.log("Hello from function expression!")
	};

	variableFunction();

	// We can also create a function expression of a named function.
	// However, to invoke the function expression, we invoke it by its variable name, not by its function name.
	// Function Expressions are always invoked (called) using the variable name.

	//a function expression of function named funcName assigned to the variable funcExpression
	let funcExpression = function funcName() {
		console.log("Hello from the other side!");
	};

	funcExpression();

	//We can also reassign declared functions and function expressions to new anonymous functions

	declaredFunction = function(){
		console.log("updated declaredFunction");
	};

	declaredFunction();

	funcExpression = function(){
		console.log("updated funcExpression");
	}

	funcExpression();


	const constantFunc = function(){
		console.log("Initialized with const!")
	};

	constantFunc();

	//However, we cannot re-assign a function expression initialized with const.

	/*constantFunc = function(){
		console.log("Cannot be reassigned!")
	}

	constantFunc();*/

	//Function Scoping

	/*
		Scope - acessibility/visibility of variables

		JS Variables has 3 types of scope:
		1. local/block scope
		2. global scope
		3. function scope


	*/
/*
	{
		let a = 1;
	}

	let a = 1;

	function sample(){
		let a = 1;
	}

*/

	{
		let localVar = "Armando Perez";
	}

	let globalVar = "Mr. Worldwide";

	//console.log(localVar);//result in an error
	console.log(globalVar);

	//Function Scope
	/*		
			JavaScript has function scope: Each function creates a new scope.
			Variables defined inside a function are not accessible (visible) from outside the function.
			Variables declared with var, let and const are quite similar when declared inside a function.
	*/


	function showNames() {

		var functionVar = "Joe";
		const functionConst = "John";
		let functionLet = "Jane";

		console.log(functionVar);
		console.log(functionConst);
		console.log(functionLet);
	};

/*	console.log(functionVar);
	console.log(functionConst);
	console.log(functionLet);*/

	showNames();


	//Nested Functions
		//You can create another function inside a function. This is called a nested function. This nested function, being inside the myNewFunction will have access to the variable, name, as they are within the same scope/code block.

	function myNewFunction(){

		let name = "Jane";

		function nestedFunction(){
			let nestedName = "John";
			console.log(name);
		}

		//console.log(nestedName);//results to an error.
			//nestedName variable, being declared in the nestedFunction cannot be accessed outside of the function it was declared in.


		nestedFunction();
	}

	myNewFunction();
	//nestedFunction();//is declared inside the myNewFunction scope



	//Function and Global Scoped Variables

	//Global Scoped Variable
	
	let globalName = "Cardo";

	function myNewFunction2(){
		let nameInside = "Hillary"

		//Variables declared Globally (outside any function) have Global scope.
		//Global variables can be accessed from anywhere in a Javascript 
		//program including from inside a function.

		console.log(globalName);
	};

	myNewFunction2();
	//console.log(nameInside);
	//nameInside is function scoped. It cannot be accessed globally.

	// Using alert()

	//alert("This will run immediately when the page loads.")
	function showSampleAlert(){
		alert("Hello, Earthlings! This is from a function!")
	}
	//showSampleAlert();

	console.log("I will only log in the console when the alert is dismissed!");

	// Using prompt()

	let samplePrompt = prompt("Enter your Name: ");

	//console.log("Hi, I am " + samplePrompt);

	//prompt returns an empty string when there is no input. or null if the user cancels the prompt()

	function printWelcomeMessage(){
		let firstName = prompt("Enter your first name: ");
		let lastName = prompt("Enter your last name: ");

		console.log("Hello, " + firstName + " " + lastName + "!");
		console.log("Welcome to my page!")
	}

	//printWelcomeMessage();

	//The Return Statement
	/*
		The return statement allows us to output a value from a function to be passed to the line/block of code that invoked/called the function
	*/

	function returnFullName(){
		return "Jeffrey" + ' ' + "Smith" + ' ' + "Bezos";
		console.log("This message will not be printed!");
	}

	/*

		In our example, the "returnFullName" function was invoked/called in the same line as declaring a variable.
		Whatever value is returned from the "returnFullName" function can be stored in a variable.

	*/


	let fullName = returnFullName();
	console.log(fullName);


	/*	
		This way, a function is able to return a value we can further use/manipulate in our program instead of only printing/displaying it in the console.
		Notice that the console.log() after the return is no longer printed in the console that is because ideally any line/block of code that comes after the return statement is ignored because it ends the function execution.
		In this example, console.log() will print the returned value of the returnFullName() function.
	*/

	console.log(returnFullName())

	/*	

		You can also create a variable inside the function to contain the result and return that variable instead. You can do this for ease of use or for readability.

	*/


	function returnFullAddress(){
		let fullAddress = {

			street : "#44 Maharlika St.",
			city: "Cainta",
			province: "Rizal"
		};

		return fullAddress;
	}

	let myAddress = returnFullAddress();
	console.log(myAddress);

	//On the other hand, when a function the only has console.log() to display its result it will return undefined instead:

	function printPlayerInfo(){

		console.log("Username: " + "dark_magician");
		console.log("Level: " + 95);
		console.log("Job: " + "Mage");

	}

	let user1 = printPlayerInfo();
	console.log(user1);//undefined


	// returns undefined because printPlayerInfo returns nothing. It only displays the details in the console. 
	// You cannot save any value from printPlayerInfo() because it does not return anything.

	// You can return almost any data types from a function.


	function returnSumOf5and10(){
		return 5 + 10;
	}

	let sumOf5And10 = returnSumOf5and10();//15
	console.log(sumOf5And10);

	let total = 100 + returnSumOf5and10();//115
	console.log(total);

	//Simulates getting an array of user names from a database

	function getGuildMembers(){

		return ["Lulu","Tristana","Teemo"];
	}

	console.log(getGuildMembers());


	//Function Naming Conventions
		//Function names should be definitive of the task it will perform. It usually contains a verb.

	function getCourses(){
		let courses = ["ReactJs 101","ExpressJs 101","MongoDB 101"];
		return courses;
	}

	let courses = getCourses();
	console.log(courses);

	//Avoid using generic names and pointless and inappropriate function names
	function get(){
		let pokemon = "pikachu";
		return pokemon;
	}

	function yeah(){
		let pink = "pink";
		return pink;
	}

	function foo(){

		return 25%5;

	};
	
	//Name your functions in small caps. Follow camelCase when naming variables and functions.
	function displayCarInfo(){
		console.log("Brand: Toyota");
		console.log("Type: Sedan");
	}

	displayCarInfo();



	//

	function getUserInfo(){

		return {
			name: "John Doe",
			age: 25,
			address: "123 Street, Quezon City",
			isMarried: false,
			petName: "Danny"

		}
	}

	let userInfo = getUserInfo();
	console.log(userInfo);