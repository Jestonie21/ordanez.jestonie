let collection = [];

// Write the queue functions below.

function print() {

    return collection;

}


function enqueue(element) {

    collection[collection.length] = element;
    return collection;

}



function dequeue() {

    if (collection.length === 0) {
    return undefined;
    }

    for (let i = 0; i < collection.length - 1; i++) {
    collection[i] = collection[i + 1];
    }

    collection.length--;

    return collection;

}


function front() {

    if (!isEmpty()) {
        return collection[0];
    }

}

function size() {

    return collection.length;

}

function isEmpty() {

    return collection.length === 0;

}



module.exports = {
    collection,
    print,
    enqueue,
    dequeue,
    front,
    size,
    isEmpty
};